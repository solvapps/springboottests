package com.solvapps.entities;

import com.solvapps.entities.Team;
import com.solvapps.helpers.AgeCalculator;
import org.hibernate.annotations.Type;


import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

/*
@NamedNativeQueries({
        @NamedNativeQuery(
                name = "getFirst",
                query = "select * from world.user limit 1",
                resultClass=User.class
        )
})
*/

@Entity
@Table(name = "users")
public class User {

    @Id
    @SequenceGenerator(name = "seq_user", initialValue = 100, allocationSize = 50)
    @GeneratedValue(generator = "seq_user")
    private Long id;

    @Column(nullable = true,length = 1000)
    private String name;
    private String surname;

    public Team getTeam() {
        return team;
    }

    @ManyToOne
    private Team team;
    @Column (nullable = true)
//    @Type(type = "date")
    private LocalDate birthdate;

    public User(Long id, String name, String surname, LocalDate birthdate, Long teamid) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.team = new Team(teamid,"");
        this.birthdate = birthdate;
    }

    public User(){

    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge(LocalDate actualDate){
        return AgeCalculator.calculateAge(birthdate, actualDate);
    }

}
