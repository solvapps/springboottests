package com.solvapps.entities;

import javax.persistence.Entity;
import java.time.LocalDate;

@Entity
//@NamedQuery(name = "User.byId", query = "from User")
public class SpecialUser extends User{

    String specialField = "";

    public SpecialUser(Long id, String name, String surname, Long teamid, LocalDate date, String specialField) {
        super(id, name, surname, date, teamid);
        this.specialField = specialField;
    }

    public SpecialUser() {
    }
}
