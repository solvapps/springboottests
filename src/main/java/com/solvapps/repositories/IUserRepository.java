package com.solvapps.repositories;

import com.solvapps.entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedNativeQuery;
import java.util.List;
import java.util.Optional;

@Repository
public interface IUserRepository extends CrudRepository<User,Long> {

    @Query("select u from User u where u.name like %?1% ")
    List<User> findByNameLike(String name);

//    public User findByName(String name);
    public List<User> findByName(String name);
    public Optional<User> getByName(String name);
    public User findByNameIgnoreCase(String name);
    public User findBySurname(String surname);
    public List<User> findByTeamId(Long teamId_anyname);
    public List<User> findByTeamName(String anyname);

}
