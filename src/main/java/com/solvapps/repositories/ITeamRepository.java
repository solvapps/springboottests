package com.solvapps.repositories;

import com.solvapps.entities.Team;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITeamRepository extends CrudRepository<Team,Long> {
}
