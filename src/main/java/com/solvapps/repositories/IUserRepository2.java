package com.solvapps.repositories;

import com.solvapps.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IUserRepository2 extends PagingAndSortingRepository<User,Long> {

    @Query("select u from User u where u.name like %?1% ")
    List<User> findByNameLike(String name);

    //Page<User> users = repository.findAll(new PageRequest(1, 20));

    public User findByName(String name);
    public User findBySurname(String surname);
    public List<User> findByTeamId(Long teamId_anyname);
    public List<User> findByTeamName(String anyname);

}
