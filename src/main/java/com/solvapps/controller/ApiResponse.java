package com.solvapps.controller;

public class ApiResponse {

    String httpcode;
    String message;
    String status;

    public ApiResponse(String httpcode, String message, String status) {
        this.httpcode = httpcode;
        this.message = message;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHttpcode() {
        return httpcode;
    }

    public void setHttpcode(String httpcode) {
        this.httpcode = httpcode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
