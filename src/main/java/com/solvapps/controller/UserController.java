package com.solvapps.controller;

import com.solvapps.dtos.UserDto;
import com.solvapps.entities.SpecialUser;
import com.solvapps.entities.Team;
import com.solvapps.entities.User;
import com.solvapps.helpers.AgeCalculator;
import com.solvapps.repositories.IUserRepository;
import com.solvapps.services.TeamService;
import com.solvapps.services.UserService;
import net.bytebuddy.description.type.TypeDescription;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;

@RestController

public class UserController {

    @Autowired
    private IUserRepository userRepository;
    @Autowired
    private UserService userService;

    @Autowired
    private TeamService temTeamService;

    @RequestMapping("callpage")
    public String callPage(){
        return "callPage";
    }

    private static final String OK = "ok";
    private static final String NOK = "nok";


    @RequestMapping("/hello")
    public Iterable<User> sayHi(){

        User user = new User();
        user.setId(02l);
        user.setName("Sporrer2");
        user.setSurname("Andreas");
//        user.setTeam(new Team(0l,"football"));

        userRepository.save(user);

        user.setName("newName");

        userService.updateUser(user);
//        Optional<User> user2 = userService.getUser((long) 0);
//        userService.deleteUser(user2.get());

        return userService.getAllUsers();

//        Iterable<User> users = userRepository.findAll();
//        return users;
    }

    @RequestMapping("/users")
    public Iterable<User> users(){
        return userService.getAllUsers();
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/usersdto")
    public Iterable<UserDto> usersdto(){
        List<User> allUsers = userService.getAllUsers();
        return getUserdtos(allUsers);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/updateuser")
    @ResponseBody
    public ApiResponse updateUser(@RequestBody UserDto userDto) throws Exception {
        //Optional<User> user = userService.getUser(id);
        Optional<User> user = userService.getUser(Long.valueOf(userDto.getId()));
        if(user.isPresent()){
            ModelMapper modelMapper = new ModelMapper();
            User updatedUser = modelMapper.map(userDto, User.class);
            setTeam(updatedUser, userDto.getTeamid());
            userService.updateUser(updatedUser);
            return new ApiResponse("200", "User updated : " + userDto.getId() + " !",OK);
        }else{
            return new ApiResponse("200", "User existiert nicht : " + userDto.getId() + " !",NOK);
        }
    }

    public void setTeam(User user, String teamId){
        Optional<Team> t = temTeamService.getTeam(Long.valueOf(teamId));
        if(t.isPresent()){
            user.setTeam(t.get());
        }else{

        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/usersdtoSuche", produces = "application/json")
    public Iterable<UserDto> usersdtoSuche(HttpServletRequest request, @RequestParam String searchname){
        String test = request.getRequestURI();
        List<User> allUsers = userService.findbyName(searchname);
        return getUserdtos(allUsers);
    }

    private List<UserDto> getUserdtos(List<User> users){
        List<UserDto> userDtos = new ArrayList<>();
        users.forEach(u -> userDtos.add(getUserdto(u)));
        return userDtos;
    }

    private UserDto getUserdto(User user){
        String teamName = "no team !";
        String teamid = "";
        if(user.getTeam() != null){
            teamName = user.getTeam().getName();
            teamid = String.valueOf(user.getTeam().getId());
        }
        return new UserDto(String.valueOf(user.getId()), user.getName(), user.getSurname(), teamid, teamName, user.getBirthdate());
    }

    @RequestMapping("/user/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public UserDto getUser(@PathVariable Long id){
        Optional<User> user = userService.getUser(id);
        if(user.isPresent()){
            return getUserdto(user.get());
        }else{
            return null;
        }
    }

    /*
    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/adduser")
    public void addUser(@RequestBody User user){
        //Optional<User> user = userService.getUser(id);
        userService.addUser(user);
    }
*/

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/adduser")
    public void addUser(@RequestBody UserDto userDto){
        //Optional<User> user = userService.getUser(id);
        userService.addUser(getUser(userDto));
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/deleteuser")
    public void deleteUser(@RequestBody UserDto userDto){
        //Optional<User> user = userService.getUser(id);
        Optional<User> userOptional = userService.getUser(Long.valueOf(userDto.getId()));
        if(userOptional.isPresent()){
            userService.deleteUser(userOptional.get());
        }
    }

    private User getUser(UserDto userDto){
        User user = new User();
        user.setName(userDto.getName());
        user.setSurname(userDto.getSurname());
        user.setBirthdate(userDto.getBirthdate());
        //u.setTeam();

        String tid = userDto.getTeamid();
        Optional<Team> t = temTeamService.getTeam(Long.valueOf(tid));
        if(t.isPresent()){
            user.setTeam(t.get());
        }

        return user;
        //return new User(Long.valueOf(userDto.getId()), userDto.getName(), userDto.getSurname(), 0l);
    }

    @RequestMapping("/addspecialuser")
    public void addSpecialUser(){
        SpecialUser specialUser = new SpecialUser(1l,"Andreas", "Sporrer",0l,LocalDate.now(), "Specialfield");
        //Optional<User> user = userService.getUser(id);
        userService.addSpecialUser(specialUser);
    }

    @RequestMapping("/getuserbyname/{name}")
    public List<User> getUserByName(@PathVariable String name){
        //Optional<User> user = userService.getUser(id);
        return userService.getUserByName(name);
    }

    @RequestMapping("/getusersbyteam/{id}")
    public List<User> getUserByTeamId(@PathVariable Long id){
        //Optional<User> user = userService.getUser(id);
        return userService.getUsersOfTeam(id);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/teams")
    public List<Team> getTeams(){
        //Optional<User> user = userService.getUser(id);
        return temTeamService.getAllTeams();
    }

    @RequestMapping("/firstuser")
    public User getFirstUser(){
        //Optional<User> user = userService.getUser(id);
        return userService.getFirstUser();
    }


    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/getmaxage")
    public int getMaxAge(){
        List<User> users = userService.getAllUsers();
        return getMaxAge(users, LocalDate.now());
    }

    public int getMaxAge(List<User> users, LocalDate actualDate){
        OptionalInt oi = users.stream().mapToInt(u -> u == null ? 0 : u.getAge(actualDate)).max();
        if(oi.isPresent()){
            return oi.getAsInt();
        }else{
            return 0;
        }
    }


    public int getMaxAgeForUsers(List<User> users, LocalDate actualDate){
        OptionalInt oi = users.stream().mapToInt(u -> u == null ? 0 : u.getAge(actualDate)).max();
        if(oi.isPresent()){
            return oi.getAsInt();
        }else{
            return 0;
        }
    }
}
