package com.solvapps.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.annotation.WebServlet;

@RestController
public class GeneralController {

    @RequestMapping("callpage2")
    @ResponseBody
    public String callPage2(){

        return "callPage";

    }

}
