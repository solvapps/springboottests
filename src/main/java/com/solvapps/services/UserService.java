package com.solvapps.services;

import com.solvapps.entities.SpecialUser;
import com.solvapps.entities.User;
import com.solvapps.repositories.IUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

@Component
public class UserService {


    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private EntityManager entityManager;

    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();
        Iterable<User> iterable = userRepository.findAll();
        iterable.forEach(users::add);
        return users;
    }

    public List<User> findbyName(String name){
        return userRepository.findByName(name);
    }

    public void updateUser(User user) {
        userRepository.save(user);
    }

    public void addUser(User user) {
        userRepository.save(user);
    }

    public void addSpecialUser(SpecialUser user) {
        userRepository.save(user);
    }

    public List<User> getUserByName(String name) {
        return userRepository.findByName(name);
    }


    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    public Optional<User> getUser(Long id) {
        return userRepository.findById(id);
    }


    public List<User> getUsersOfTeam(Long teamId_anyname) {
        return userRepository.findByTeamId(teamId_anyname);
    }

    public List<User> getUsersOfTeam(String teamName) {
        return userRepository.findByTeamName(teamName);
    }


    public User getFirstUser() {
        Query q = entityManager.createNativeQuery("select * from world.user limit 1");
        Object user = q.getSingleResult();
        return (User) user;
    }



}
