package com.solvapps.services;

import com.solvapps.repositories.ITeamRepository;
import com.solvapps.entities.Team;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TeamService {

    @Autowired
    private ITeamRepository teamRepository;

    public List<Team> getAllTeams(){
        List<Team> teams = new ArrayList<>();
        teamRepository.findAll().forEach(teams::add);
        return teams;
    }

    public void updateTeam(Team team){
        teamRepository.save(team);
    }

    public void addTeam(Team team){
        teamRepository.save(team);
    }


    public void deleteTeam(Team team){
        teamRepository.delete(team);
    }

    public Optional<Team> getTeam(Long id){
        return teamRepository.findById(id);
    }


}
