export interface ApiResponseDto {

  httpcode? : string;
  message? : string;
  status : string;

}
