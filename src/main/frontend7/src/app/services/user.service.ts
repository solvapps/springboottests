import { UserDto } from "../../entities/UserDto";
import {Component, Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
// import 'rxjs/add/operator/catch';
import {Observable} from "rxjs/index";
import { map, catchError } from 'rxjs/operators';
import {ApiResponseDto} from "../../entities/ApiResponseDto";

// import { catch } from 'rxjs/operators';

@Injectable()
export class UserService{

  http: HttpClient;

  private _addurl : string = "http://localhost:8080/user/";

  constructor(http: HttpClient){
    this.http = http;
  }

  public saveUser(userdto: UserDto){

    let url = 'http://localhost:8080/adduser';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('add : ', userdto.name + ' ' + userdto.surname);
    return this.http.post<UserDto>(url, userdto, {headers}).pipe();

  }

  public updateUser(userdto: UserDto){

    let url = 'http://localhost:8080/updateuser';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('update : ', userdto.name + ' ' + userdto.surname);
    return this.http.post<ApiResponseDto>(url, userdto, {headers}).pipe();

  }

  public getUser(userid: string): Observable<UserDto> {
    let params = new HttpParams().set('id', userid);
    // return this.http.get<UserDto>(this._addurl).
    return this.http.get<UserDto>(this._addurl + userid , { params});
  }

  public deleteUser(userdto: UserDto){

    // let url = 'http://localhost:8080/deleteuser';
    let url = 'http://localhost:8080/deleteuser';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('delete : ', userdto.name + ' ' + userdto.surname);
    console.debug(JSON.stringify(userdto));
    return this.http.post<UserDto>(url, userdto, {headers}).pipe();

  }

  getUserList(){
    return this.http.get<UserDto[]>("http://localhost:8080/usersdto");
    // return this.http.get<UserDto[]>(AppSettings.);
  }

  getFilteredUsers(filter : string){
    let params = new HttpParams().set('searchname', filter);
    return this.http.get<UserDto[]>("http://localhost:8080/usersdtoSuche", { params});
  }


  getmaxage(){
    return this.http.get<number>("http://localhost:8080/getmaxage");
    // return this.http.get<UserDto[]>(AppSettings.);
  }


}
