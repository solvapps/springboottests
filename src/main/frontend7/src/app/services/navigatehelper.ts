import {Injectable} from "@angular/core";
import {Router} from "@angular/router";

@Injectable()
export class NavigateHelper {

  router : Router;

  backtoUserlist(){
    this.router.navigateByUrl('/userliste');
  }

  constructor(_router: Router) {
    this.router = _router;
  }

}
