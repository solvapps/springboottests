import { BrowserModule } from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UserlistComponent } from './userlist/userlist.component';
import { UserformularComponent } from './userformular/userformular.component';

import { HttpClientModule  } from "@angular/common/http";
import { TeamlistComponent } from './teamlist/teamlist.component';
import { UserformularvalidationComponent } from './userformularvalidation/userformularvalidation.component';

import { MatTableModule } from '@angular/material'
import {ErrorhandlerService} from "./services/errorhandler.service";


const appRoutes: Routes = [
  { path: 'userform', component: UserformularComponent },
  { path: 'userformvalidation', component: UserformularvalidationComponent },
  { path: 'newuserform', component: UserformularvalidationComponent },
  { path: 'updateuserform/:id/:action', component: UserformularvalidationComponent },
  { path: 'userliste',      component: UserlistComponent },
  { path: 'teamliste',      component: TeamlistComponent },
  { path: '',      component: UserlistComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UserlistComponent,
    UserformularComponent,
    TeamlistComponent,
    UserformularvalidationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    [RouterModule.forRoot(appRoutes,{enableTracing: true})],
    MatTableModule
  ],
  providers: [ErrorhandlerService,
    { provide: ErrorHandler, useClass: ErrorhandlerService }],
  exports: [ RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule {


}
