import { Component, OnInit } from '@angular/core';
import {TeamService} from "../services/team.service";
import {TeamDto} from "../../entities/TeamDto";

@Component({
  selector: 'app-teamlist',
  templateUrl: './teamlist.component.html',
  styleUrls: ['./teamlist.component.css'],
  /*providers:[TeamService]*/ // Das ist allgemein in appcomponent provided.
})
export class TeamlistComponent implements OnInit {

  teams : Array<TeamDto> = [];
  constructor(private teamservice : TeamService) {

    //this.teamservice = teamservice;
    teamservice.getTeamList().subscribe(value => this.teams = value)

  }

  ngOnInit() {
  }


  getAllTeams()
  {
    this.teamservice.getTeamList().subscribe(value => this.teams = value);
    console.info("Hallo test !" + " " + this.teams.length);
  }


}
