import { Component } from '@angular/core';
import {UserService} from "./services/user.service";
import {TeamService} from "./services/team.service";
import {NavigateHelper} from "./services/navigatehelper";
import {ColorHelper} from "./services/colorhelper";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService, TeamService, NavigateHelper, ColorHelper]
})
export class AppComponent {
  title = 'app';
}
