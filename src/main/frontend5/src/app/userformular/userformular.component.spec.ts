/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UserformularComponent } from './userformular.component';

describe('UserformularComponent', () => {
  let component: UserformularComponent;
  let fixture: ComponentFixture<UserformularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserformularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserformularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
