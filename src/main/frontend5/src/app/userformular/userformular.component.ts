import { Component, OnInit } from '@angular/core';
import {UserDto} from "../../entities/UserDto";
import {Headers, Http, URLSearchParams} from "@angular/http";
import {UserService} from "../services/user.service";
import {TeamDto} from "../../entities/TeamDto";
import {TeamService} from "../services/team.service";
import {Router} from '@angular/router';
import {NavigateHelper} from "../services/navigatehelper";
import {ColorHelper} from "../services/colorhelper";

@Component({
  selector: 'app-userformular',
  templateUrl: './userformular.component.html',
  styleUrls: ['./userformular.component.css']
})
export class UserformularComponent implements OnInit {
  //http: Http;
  // userService:UserService;
  user: UserDto = {
    name : "",
    surname : "",
    teamid : "",
    teamname : ""
  };

  teams : Array<TeamDto> = [];
  teamid : string;
  // router : Router;
  navHelper : NavigateHelper;
  colorhelper: ColorHelper;

  constructor(colohelper : ColorHelper, navhelper :  NavigateHelper/*, _router: Router*/, private userService:UserService, private teamService: TeamService) {
    this.teamService.getTeamList().subscribe(value => this.teams = value);
    //this.router = _router;
    this.navHelper = navhelper;
    this.colorhelper = colohelper;
    //this.colorhelper.getRed();
  }

  add(){
    this.user.teamid = this.teamid;
    console.debug("team id from user : " + this.user.teamid);
    console.debug(JSON.stringify(this.user));
    this.userService.saveUser(this.user).subscribe(value => this.backtoUserlist());
  }

  backtoUserlist(){
    this.navHelper.backtoUserlist();
    //this.router.navigateByUrl('/userliste');
  }

  ngOnInit() {
  }

}
