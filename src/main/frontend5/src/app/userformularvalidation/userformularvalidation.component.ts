import { Component, OnInit } from '@angular/core';
import {NavigateHelper} from "../services/navigatehelper";
import {ColorHelper} from "../services/colorhelper";
import {TeamDto} from "../../entities/TeamDto";
import {UserDto} from "../../entities/UserDto";
import {UserService} from "../services/user.service";
import {TeamService} from "../services/team.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-userformularvalidation',
  templateUrl: './userformularvalidation.component.html',
  styleUrls: ['./userformularvalidation.component.css']
})
export class UserformularvalidationComponent implements OnInit {

  //http: Http;
  // userService:UserService;
  user: UserDto = {
    name : "",
    surname : "",
    teamid : "",
    teamname : ""
  };

  userid : string;
  action : string;

  teams : Array<TeamDto> = [];
  teamid : string;
  // router : Router;
  navHelper : NavigateHelper;
  colorhelper: ColorHelper;

  constructor(colohelper : ColorHelper,
              navhelper :  NavigateHelper/*, _router: Router*/,
              private userService:UserService,
              private teamService: TeamService,
              private activeroute: ActivatedRoute) {
    this.teamService.getTeamList().subscribe(value => this.teams = value);
    //this.router = _router;
    this.navHelper = navhelper;
    this.colorhelper = colohelper;
    //this.colorhelper.getRed();
  }

  add(){
    this.user.teamid = this.teamid;
    console.debug("team id from user : " + this.user.teamid);
    console.debug(JSON.stringify(this.user));
    this.userService.saveUser(this.user).subscribe(value => this.backtoUserlist());
  }

  update(){
    this.user.teamid = this.teamid;
    console.debug("team id from user : " + this.user.teamid);
    console.debug(JSON.stringify(this.user));
    this.userService.updateUser(this.user).subscribe(value => this.backtoUserlist());
  }

  backtoUserlist(){
    this.navHelper.backtoUserlist();
    //this.router.navigateByUrl('/userliste');
  }

  ngOnInit() {
    var userid = -1;
    this.activeroute.params.subscribe(value => {
      this.userid = value['id'];
      this.action = value['action'];
      if(this.action === 'update'){
        console.debug("update action !");
        this.userService.getUser(value['id']).subscribe(userparameter => {
          this.user = userparameter;
          console.debug("teamid == " + this.user.teamid);
          console.debug("name == " + this.user.name);
          this.teamid = this.user.teamid;
          console.debug("teamid == " + this.teamid);
        });
      }else{
        console.debug("no action !");
        this.action = 'add';
      }

    });
    console.debug("in ngInit von UserformularvalidationComponent : " + this.userid);
  }




}
