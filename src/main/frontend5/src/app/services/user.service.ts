import { UserDto } from "../../entities/UserDto";
import { Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";

@Injectable()
export class UserService{

  http: HttpClient;

  constructor(http: HttpClient){
    this.http = http;
  }

  public saveUser(userdto: UserDto){

    let url = 'http://localhost:8080/adduser';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('add : ', userdto.name + ' ' + userdto.surname);
    return this.http.post<UserDto>(url, userdto, {headers}).pipe();

  }

  public updateUser(userdto: UserDto){

    let url = 'http://localhost:8080/updateuser';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('update : ', userdto.name + ' ' + userdto.surname);
    return this.http.post<UserDto>(url, userdto, {headers}).pipe();

  }

  public getUser(userid: string) {
    let params = new HttpParams().set('id', userid);
    return this.http.get<UserDto>("http://localhost:8080/user/" + userid , { params});
  }

  public deleteUser(userdto: UserDto){

    // let url = 'http://localhost:8080/deleteuser';
    let url = 'http://localhost:8080/deleteuser';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('delete : ', userdto.name + ' ' + userdto.surname);
    console.debug(JSON.stringify(userdto));
    return this.http.post<UserDto>(url, userdto, {headers}).pipe();

  }

  getUserList(){
    return this.http.get<UserDto[]>("http://localhost:8080/usersdto");
    // return this.http.get<UserDto[]>(AppSettings.);
  }

  getFilteredUsers(filter : string){
    let params = new HttpParams().set('searchname', filter);
    return this.http.get<UserDto[]>("http://localhost:8080/usersdtoSuche", { params});
  }


  getmaxage(){
    return this.http.get<number>("http://localhost:8080/getmaxage");
    // return this.http.get<UserDto[]>(AppSettings.);
  }


}
