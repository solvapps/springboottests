
import { Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, } from "@angular/common/http";
import {TeamDto} from "../../entities/TeamDto";


@Injectable()
export class TeamService{

  http: HttpClient;

  constructor(http: HttpClient){
    this.http = http;
  }


  public saveTeam(teamdto: TeamDto){

    let url = 'http://localhost:8080/addteam';
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    console.info('add : ', teamdto.name );
    return this.http.post<TeamDto>(url, teamdto, {headers}).pipe();

  }


  getTeamList(){
    return this.http.get<TeamDto[]>("http://localhost:8080/teams");
    // return this.http.get<UserDto[]>(AppSettings.);
  }

}
