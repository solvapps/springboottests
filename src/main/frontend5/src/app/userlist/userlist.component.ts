import { Component, OnInit } from '@angular/core';
import {Http, Headers, URLSearchParams} from '@angular/http'
import {UserDto} from "../../entities/UserDto";
import {UserService} from "../services/user.service";
import 'rxjs/add/operator/map'
import {Observable} from "rxjs/Observable";
import {MatTableDataSource} from "@angular/material";
//import {Sort} from '@angular/material';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})

export class UserlistComponent {

  userdtos : Array<UserDto> = [];
  userservice : UserService;
  maxage : number;

  filterName : string;

  addenebled : boolean = true;
  private z: Observable<Array<UserDto>>;


  constructor(userservice:UserService) {
    this.userservice = userservice;
    this.filterName = "";
    this.refreshPage();
  }

  getmaxAge(){
    this.userservice.getmaxage().subscribe(value => {
      this.maxage = value;

    });
  }

  refreshPage(){
    this.search();
    this.getmaxAge();

  }

  search(){
    if(this.filterName == ""){
      this.getUsersAsList('');
    }else{
      this.getUsersAsList(this.filterName);
    }

  }

  deleteUser(userdto: UserDto)
  {
    console.debug("deleting User ! " + userdto.id);
    this.userservice.deleteUser(userdto).subscribe(value => this.refreshPage());
  }

  getUsersAsList(filter: string)
  {
    if(filter == ''){
      this.userservice.getUserList().subscribe(value => this.userdtos = value);
    }else{
      this.userservice.getFilteredUsers(filter).subscribe(value => this.userdtos = value);
    }
    console.info("Hallo test !" + " " + this.userdtos.length);
  }

}
