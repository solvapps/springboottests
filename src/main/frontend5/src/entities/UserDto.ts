export interface UserDto {

  id? : string;
  name : string;
  surname : string;
  birthdate? : string;
  teamid : string;
  teamname : string;

}
