import { Component, OnInit } from '@angular/core';
//import {Http, Headers, URLSearchParams} from '@angular/http'
//import {Http, Headers, URLSearchParams} from '@angular/common/http'
import {UserDto} from "../../entities/UserDto";
import {UserService} from "../services/user.service";
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from "@angular/common/http";
import {map} from "rxjs/internal/operators";
import {Observable} from "rxjs/index";

// fontend is old
// frontend5 is the new one.  ok



@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent {

  userdtos : Array<UserDto> = [];
  selectedUser : UserDto;

  http: HttpClient;


  filterName : string;

  addenebled : boolean = true;

  myObservable : Observable<Array<UserDto>>;


  constructor(http: HttpClient) {
    this.http = http;
    this.getAll();
    this.filterName = "bla";
  }

  //function showResponse(resp: Response) { console.debug('Status-Code', resp.status); console.debug('Status-Text', resp.statusText); console.debug('Content-Type', resp.headers.get('Content-Type')); console.debug('Alle Header-Namen', resp.headers.keys()); console.debug('Nutzdaten als String', resp.text()); }

  search(){

    console.info("Hallo test !");

    if(this.filterName == ""){
      this.getAll();
    }else{
      let url = 'http://localhost:8080/usersdtoSuche';
      let headers = new HttpHeaders();
      headers.set('Accept', 'application/json');
      let search = new URLSearchParams()

      const options = this.filterName ?
        { params: new HttpParams().set('searchname', this.filterName) } : {};

      // Old that worked.
      //this.http.get(url,{ search, headers}).map(resp => resp.json()).subscribe(users => this.userdtos = users);

      this.http.get(url,options).pipe(map(res => res.valueOf()))


//      this.myObservable = this.http.get(url,options).subscribe(users => this.userdtos = users);
      //this.myObservable.subscribe(users => this.userdtos = users);



      //map(resp => resp.json()).subscribe(users => this.userdtos = users);
    }

  }

  getAll(){
    let url = 'http://localhost:8080/usersdto';
    let headers = new Headers();
    headers.set('Accept', 'application/json');
    //this.http.get(url,headers).map(resp => resp.json()).subscribe(users => this.userdtos = users);
    this.http.get(url).pipe(map(res => res.valueOf()));
//let me check

    //this.http.get(url).subscribe(users :  => this.userdtos = users)
  }



}
