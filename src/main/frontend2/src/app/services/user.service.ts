import { UserDto } from "../../entities/UserDto";
import {/*Class,*/ Component, Injectable} from "@angular/core";
//import { Observable} from "rxjs/Observable";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {catchError} from "rxjs/internal/operators";
import {throwError} from "rxjs/index";


@Injectable()
export class UserService{

  http: HttpClient;

  constructor(http: HttpClient){

    this.http = http;

  }

  public saveUser(userdto: UserDto){

    let url = 'http://localhost:8080/adduser';
    let headers = new HttpHeaders();

    headers.set('Accept', 'application/json');

//    return this.http.post(url,userdto,{headers}).map(resp2 => resp2.json());
    return this.http.post(url,userdto,{headers}).pipe(
      catchError(this.handleError)
    );


  }


  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };


}


//public save(flight: Flight): Observable<Flight> { let url = "…"; let headers = new Headers(); headers.set('Accept', 'application/json'); return this .http .post(url, flight, { headers }) .map(resp => resp.json()); }

