import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UserlistComponent } from './userlist/userlist.component';
import { UserformularComponent } from './userformular/userformular.component';


const appRoutes: Routes = [
  { path: 'userform', component: UserformularComponent },
  { path: 'userliste',      component: UserlistComponent },
  { path: '',      component: UserlistComponent }
  //,{ path: '**', component: PageNotFoundComponent }
];




@NgModule({
  declarations: [
    AppComponent,
    UserlistComponent,
    UserformularComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule, [RouterModule.forRoot(appRoutes,{enableTracing: true})]
  ],
  providers: [],
  exports: [ RouterModule],
  bootstrap: [AppComponent]
})
export class AppModule {


}
