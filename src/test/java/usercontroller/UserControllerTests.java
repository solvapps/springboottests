package usercontroller;

import com.solvapps.controller.UserController;
import com.solvapps.entities.User;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Arrays;

public class UserControllerTests {

    @Test
    public void maxAgeTest(){
        UserController userController = new UserController();
        User u1 =new User(0l, "n1", "s1", LocalDate.of(1999, 01, 01),0l);
        User u2 =new User(1l, "n2", "s2", LocalDate.of(2009, 01, 01),0l);
        float average = userController.getMaxAge(Arrays.asList(u1, u2), LocalDate.of(2019, 1,1 ));
        Assert.assertEquals(20f, average, 1); //false
    }
    @Test
    public void maxAgeTest2(){
        UserController userController = new UserController();
        User u1 =new User(0l, "n1", "s1", LocalDate.of(2009, 01, 01),0l);
        User u2 =new User(1l, "n2", "s2", LocalDate.of(2009, 01, 01),0l);
        float average = userController.getMaxAge(Arrays.asList(u1, u2, null), LocalDate.of(2019, 1,1 ));
        Assert.assertEquals(10f, average, 1); //false
    }

}
