package com.solvapps.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solvapps.controller.UserController;
import com.solvapps.entities.User;
import com.solvapps.repositories.IUserRepository;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompare;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.springframework.data.repository.init.ResourceReader.Type.JSON;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class ControllerTests {

    @MockBean
    private UserService userService;

    @MockBean
    private IUserRepository userRepository;

    @MockBean
    private TeamService temTeamService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    public void FetchUserTest() throws Exception {

        Mockito.when(userService.getAllUsers()).thenReturn(Arrays.asList(new User[] {new User(0l,"Spor","And",LocalDate.now(),0l)}));

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/users").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String actual = result.getResponse().getContentAsString();
        String expected = "[{\"id\":0,\"name\":\"Spor\",\"surname\":\"And\",\"team\":{\"id\":0,\"name\":\"\"},\"birthdate\":[2019,2,5]}]";

        JSONAssert.assertEquals(expected, actual, JSONCompareMode.LENIENT);
    }


    @Test
    public void UpdateUserTestUserNotFound() throws Exception {

        Mockito.when(userService.getUser(1l)).thenReturn(Optional.empty());

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post(
                "/updateuser").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(sampleUser())).accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String actual = result.getResponse().getContentAsString();
        String expected = "{\"httpcode\":\"200\",\"message\":\"User existiert nicht : 0 !\",\"status\":\"nok\"}";

        JSONAssert.assertEquals(expected, actual, JSONCompareMode.LENIENT);
    }

    public User sampleUser(){
        return new User(0l,"Spor","And",LocalDate.now(),0l);
    }


}
