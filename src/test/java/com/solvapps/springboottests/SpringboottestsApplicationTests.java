package com.solvapps.springboottests;

import com.solvapps.entities.Team;
import com.solvapps.services.TeamService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringboottestsApplicationTests {

    private static final Logger logger = LoggerFactory.getLogger(SpringboottestsApplicationTests.class);

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private ApplicationContext ctx;


    @Test
    public void contextLoads() {

//        Object r = ctx.getBean("wegwer");
        Arrays.asList(ctx.getBeanDefinitionNames()).forEach(System.out::println);
        System.out.println("Ende ");
        TeamService teamService = (TeamService) ctx.getBean("teamService");
        List<Team> l = teamService.getAllTeams();
        Assert.assertEquals(1,l.size());
        logger.info("just a test info log");

    }

}
