package com.solvapps.springboottests.subtests;

import com.solvapps.entities.User;
import com.solvapps.repositories.IUserRepository;
import com.solvapps.repositories.IUserRepository2;
import com.solvapps.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.JsonTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
//@JsonTest
@SpringBootTest
public class DbTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private IUserRepository iUserRepository;

    @Autowired
    private IUserRepository2 iUserRepository2;
    
    @Autowired
    private UserService userService;

    @Test
    public void myTest(){
        Query q = entityManager.createNativeQuery("select * from world.users limit 1");
        Object user = q.getSingleResult();
        System.out.println(user);
    }

    @Test
    public void myTest2(){
        List<User> d = userService.getAllUsers();
        Assert.assertEquals(true,d.size()>0);
    }

}
