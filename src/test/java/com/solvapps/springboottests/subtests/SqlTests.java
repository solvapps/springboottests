package com.solvapps.springboottests.subtests;

import com.solvapps.entities.User;
import com.solvapps.repositories.IUserRepository;
import com.solvapps.repositories.IUserRepository2;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.NamedNativeQuery;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SqlTests {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private IUserRepository iUserRepository;

    @Autowired
    private IUserRepository2 iUserRepository2;

    @Test
    public void myTest(){
        Query q = entityManager.createNativeQuery("select * from world.user limit 1");
        Object user = q.getSingleResult();
        System.out.println(user);
    }
//    @Test
//    public void myCriteriaTest(){
//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<User> userQuery = cb.createQuery(User.class);
//        userQuery.select()
//        Object user = q.getSingleResult();
//        System.out.println(user);
//    }

    @Test
    public void myTest2(){
        Query q = entityManager.createNativeQuery("select * from world.user where name = ?");
        q.setParameter(1,"newName");
        Object user = q.getSingleResult();
        System.out.println(user);
    }

//    @Test
//    public void myNamequeryTest(){
//        Query q = entityManager.createNamedQuery ("getFirst", User.class);
//        q.setParameter(1,"newName");
//        Object user = q.getSingleResult();
//        System.out.println(user);
//    }

    @Test
    public void myNamequeryTest2(){
        List<User> users = iUserRepository.findByNameLike("new");

        System.out.println(users.size());
    }

    @Test
    public void myNamequeryTest5(){
//        User user = iUserRepository.findByNameIgnoreCase("new");
        Optional<User> user = iUserRepository.getByName("newNam");

        System.out.println(user.get());
    }

    @Test
    public void myNamequeryTest3(){
        Page<User> users = iUserRepository2.findAll(new PageRequest(1,2));

        System.out.println(users.getTotalElements());
    }

    @Test
    public void myNamequeryTest4(){
//        Page<User> users = iUserRepository.findAll(new PageRequest(1,2));
//
//        System.out.println(users.getTotalElements());
    }


}
